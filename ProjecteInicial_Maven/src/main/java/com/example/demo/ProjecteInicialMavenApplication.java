package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjecteInicialMavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjecteInicialMavenApplication.class, args);
	}

}
